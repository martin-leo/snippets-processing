/* Export au format PNG et PDF

Pour l'export au format PNG, c'est simple :
le sketch est exporté au format PNG
lorsque la touche 'S' est relevée.

Pour l'export au format PDF, c'est plus compliqué :
il faut signifier à Processing de commencer à enregistrer
ce qui est dessiné dans le sketch, puis d'arrêter
et d'exporter ce qu'enregistré au format PDF.

Par défaut ici, l'enregistrement en PDF est lancé dès le début du sketch.
*/

// on importe la librairie PDF
import processing.pdf.*;

// déclaration des variables utiles
String  racine_nom_fichier;
Boolean etat_export_pdf;
Boolean commencer_a_enregistrer_au_lancement_du_sketch;

void setup() {
  // réglage du préfixe des fichiers exportés
  racine_nom_fichier = "export";
  // paramètre : commence-t-on à enregistrer en PDF dès le lancement du sketch ?
  commencer_a_enregistrer_au_lancement_du_sketch = true;
  
  // si l'on commence à enregistrer dès le lancement,
  // on assigne true à etat_export_pdf
  // et sinon false
  if (commencer_a_enregistrer_au_lancement_du_sketch == true) {
    demarrer_enregistrement_pdf();
  } else {
    etat_export_pdf = false;
  }
}

void draw() {
  // code d'exemple
  line(mouseX, mouseY, 50, 50);
}

void keyReleased() {
  /*  Cette fonction est appelée à chaque fois qu'une touche du clavier est relâchée.
      Veillez à ne pas avoir deux fonction keyReleased.
      Voir https://processing.org/reference/keyReleased_.html */
      
  // si la touche 's' est relevée
  if (keyCode == 83) {
    sauvegarder_png();
    
  // si la touche 'p' est relevée
  } else if (keyCode == 80) {
    // si l'on est pas déjà en train d'enregistrer un PDF
    // on lance un enregistrement
    if (etat_export_pdf == false) {
      demarrer_enregistrement_pdf();
    // si on était en train d'enregistrer, on arrête et on exporte
    } else {
      terminer_enregistrement_pdf();
    }
  }
}

/* -----
  fonctions relatives à l'export PDF et PNG
   ----- */

void demarrer_enregistrement_pdf() {
  etat_export_pdf = true;
  beginRecord(PDF, nom_fichier() + ".pdf"); 
  print("démarrage de l'enregistrement au format PDF\n");
}

void terminer_enregistrement_pdf() {
  etat_export_pdf = false;
  endRecord();
  print("fin de l'enregistrement au format PDF\n");
}

void sauvegarder_png() {
  /*  fonction de sauvegarde au format PNG */
  
  // sauvegarde avec save : https://processing.org/reference/save_.html
  save(nom_fichier() + ".png");
}

String nom_fichier() {
  // vous pouvez paramétrer ci-dessous la racine du nom de fichier
  
  // timestamp
  String timestamp = str( year() ) + '-' + str( month() ) + '-' + str( day() ) + '-' + str( hour() ) + '-' + str( minute() ) + '-' + str( second() ) + '-' + str( millis() );
  
  return racine_nom_fichier + '-' + timestamp;
}

/* -----
  fin des fonctions relatives à l'export PDF et PNG
   ----- */